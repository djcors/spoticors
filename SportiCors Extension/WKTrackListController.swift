
//
//  TrackListController.swift
//  SpotiCors
//
//  Created by Jonathan on 27/02/16.
//  Copyright © 2016 Jonathan. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class WKTrackListController: WKInterfaceController, WKExtensionDelegate, WCSessionDelegate  {

    @IBOutlet var TrackListTable: WKInterfaceTable!
    @IBOutlet var cover: WKInterfaceImage!
    

    @IBOutlet var notracks: WKInterfaceLabel!
    var Pid:String!
    var owner:Dictionary<String,AnyObject>!
    var Uid:String!
    var get_url:NSURL!
    var token:String!
    let myDelegate = WKExtension.sharedExtension().delegate as! ExtensionDelegate
    var uris_string = [String]()
    
    var session : WCSession!
    
    var items:[[String: AnyObject]]!
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        self.session = self.myDelegate.session
        self.session.activateSession()
        
        let title = context!["name"] as! String
        setTitle(title)
        self.Pid = context!["id"] as! String
        self.owner = context!["owner"] as! Dictionary<String,AnyObject>
        self.Uid = self.owner["id"] as! String
        var url = "https://api.spotify.com/v1/users/\(self.Uid!)/playlists/\(self.Pid!)"
        url = url.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLFragmentAllowedCharacterSet())!
        self.get_url = NSURL(string: url)
        self.token = self.myDelegate.token
        let tra = context!["tracks"] as! Dictionary<String,AnyObject>
        let total:Int! = Int(tra["total"]! as! NSNumber)
        self.notracks.setHidden(true)
        if total > 0{
            self.requestPlayList(self.token)
        }else{
            self.notracks.setHidden(false)
            self.notracks.setText("No hay canciones")
        }
    }
    
    
    func requestPlayList(token:String!){
        let sessionApi = NSURLSession.sharedSession()
        let muableRequest = NSMutableURLRequest(URL: self.get_url!)
        muableRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        muableRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        muableRequest.HTTPMethod = "GET"
        
        let task = sessionApi.dataTaskWithRequest(muableRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil){
                print(error?.localizedDescription)
                return
            }
            let nsdata:NSData =  NSData(data:data!)
            do {
                let json = try NSJSONSerialization.JSONObjectWithData(nsdata, options: .AllowFragments)
                let tracks = json["tracks"] as! Dictionary<String,AnyObject>
                self.items = tracks["items"] as! [[String: AnyObject]]
                self.TrackListTable.setNumberOfRows(self.items.count, withRowType:"cell")
                for (var i = 0; i < self.items.count; i++) {
                    let track = self.items[i]["track"]! as! Dictionary<String,AnyObject>
                    let cell = self.TrackListTable.rowControllerAtIndex(i) as! TrackListRow
                    cell.trackname.setText(String(track["name"]!))
                    self.uris_string.append(track["uri"]! as! String)
                }
                let get_image:NSArray! = json["images"] as! NSArray!
                let image_dic = get_image![0] as! Dictionary<String,AnyObject>
                let image_url:NSURL! = NSURL(string: String(image_dic["url"]!))!
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {

                NSURLSession.sharedSession().dataTaskWithURL(image_url) { data, response, error in
                    if (data != nil && error == nil) {
                        let image = UIImage(data: data!)
                        let imageSize = image!.size
                        let scale: CGFloat = 0.3 // Scale factor
                        
                        let reducedSize = CGSizeMake(imageSize.width * scale,
                            imageSize.height * scale);
                        UIGraphicsBeginImageContext(reducedSize)
                        
                        image!.drawInRect(CGRectMake(0, 0, reducedSize.width,
                            reducedSize.height))
                        
                        let compressedImage = UIGraphicsGetImageFromCurrentImageContext()
                        
                        dispatch_async(dispatch_get_main_queue()) {
                            self.cover.setImage(compressedImage)
                        }
                    }
                }.resume()
                })
                
                
            } catch {
                print(error)
            }
        })
        task.resume()
    }
    
    
    override func table(table: WKInterfaceTable, didSelectRowAtIndex rowIndex: Int) {
        /*let messageToSend = ["uris": self.uris_string, "index":rowIndex, "track":self.items[rowIndex]["track"]]
        print(messageToSend)
        session.sendMessage(messageToSend as! [String : AnyObject], replyHandler: { (respuesta:[String : AnyObject]) -> Void in
            }) { (error:NSError!) -> Void in
                print(error.localizedDescription)
            }
        */
    }
    
    override func contextForSegueWithIdentifier(segueIdentifier: String, inTable table: WKInterfaceTable, rowIndex: Int) -> AnyObject? {
        //self.pushControllerWithName("WKTrackListController", context: self.items[rowIndex])
        
        let context = ["uris":self.uris_string, "index":rowIndex,"track":self.items[rowIndex]["track"]! ] as Dictionary<String,AnyObject>
        
        return context
    }


    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
