//
//  MyMusicController.swift
//  SpotiCors
//
//  Created by Jonathan on 5/03/16.
//  Copyright © 2016 Jonathan. All rights reserved.
//

import WatchKit
import Foundation


class MyMusicController: WKInterfaceController {

    
    @IBOutlet var tracktable: WKInterfaceTable!
    
    let url = NSURL(string: "https://api.spotify.com/v1/me/tracks?limit=50")
    var token:String!
    let myDelegate = WKExtension.sharedExtension().delegate as! ExtensionDelegate
    var items:[[String: AnyObject]]!
    var uris_string = [String]()
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        setTitle("My Music")
        self.token = self.myDelegate.token
        self.requestMe(self.token)

    }
    
    
    func requestMe(token:String!){
        let sessionApi = NSURLSession.sharedSession()
        let muableRequest = NSMutableURLRequest(URL: url!)
        muableRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        muableRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        muableRequest.HTTPMethod = "GET"
        
        let task = sessionApi.dataTaskWithRequest(muableRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil){
                print(error?.localizedDescription)
                return
            }
            let nsdata:NSData =  NSData(data:data!)
            do {
                let json = try NSJSONSerialization.JSONObjectWithData(nsdata, options: .AllowFragments)
                //let total:Int = json["total"] as! Int
                self.items = json["items"] as! [[String: AnyObject]]
                self.tracktable.setNumberOfRows(self.items.count, withRowType:"cell")
                for (var i = 0; i < self.items.count; i++) {
                    let track = self.items[i]["track"]! as! Dictionary<String,AnyObject>
                    let cell = self.tracktable.rowControllerAtIndex(i) as! MyMusicRow
                    cell.trackname.setText(String(track["name"]!))
                    self.uris_string.append(track["uri"]! as! String)
                }
            } catch {
                print(error)
            }
        })
        task.resume()
    }
    
    override func contextForSegueWithIdentifier(segueIdentifier: String, inTable table: WKInterfaceTable, rowIndex: Int) -> AnyObject? {
        //self.pushControllerWithName("WKTrackListController", context: self.items[rowIndex])
        
        let context = ["uris":self.uris_string, "index":rowIndex,"track":self.items[rowIndex]["track"]! ] as Dictionary<String,AnyObject>
        
        return context
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
