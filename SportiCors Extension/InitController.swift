//
//  InterfaceController.swift
//  SportiCors Extension
//
//  Created by Jonathan on 22/02/16.
//  Copyright © 2016 Jonathan. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InitController: WKInterfaceController, WCSessionDelegate {

    @IBOutlet var user_name: WKInterfaceLabel!
    @IBOutlet var buscar: WKInterfaceButton!
    @IBOutlet var musica: WKInterfaceButton!
    @IBOutlet var playlists: WKInterfaceButton!
    
    @IBOutlet var Gsearch: WKInterfaceGroup!
    @IBOutlet var Gmusic: WKInterfaceGroup!
    @IBOutlet var Gplaylist: WKInterfaceGroup!
    
    let url = NSURL(string: "https://api.spotify.com/v1/me/")
    var token:String!
    let myDelegate = WKExtension.sharedExtension().delegate as! ExtensionDelegate
    var session : WCSession!
    
    let userDefaults = NSUserDefaults.standardUserDefaults()
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        self.session = self.myDelegate.session
        self.session.activateSession()
        
        let messageToSend = ["lat":["hola"]]
        self.session.sendMessage(messageToSend, replyHandler: { (respuesta:[String : AnyObject]) -> Void in
            self.token = respuesta["token"] as! String
            }) { (error:NSError!) -> Void in
                print(error.localizedDescription)
        }
        
        
        if let sessionObj:AnyObject = userDefaults.objectForKey("token"){
            let data = sessionObj as! NSData
            let token = NSKeyedUnarchiver.unarchiveObjectWithData(data)
            self.token = token as! String
            self.myDelegate.token = self.token
        }
        if self.token == nil{
            self.token = self.myDelegate.token
        }
        

        setTitle("SpotiCors")
        self.Gmusic.setHidden(true)
        self.Gsearch.setHidden(true)
        self.Gplaylist.setHidden(true)
    }
    @IBAction func reload() {
        let messageToSend = ["renew":"renew"]
        self.session.sendMessage(messageToSend, replyHandler: { (respuesta:[String : AnyObject]) -> Void in
            self.get_user()
        }) { (error:NSError!) -> Void in
            print(error.localizedDescription)
        }
    }

    override func willActivate() {
        super.willActivate()
        if self.token != nil{
            self.get_user()
        }
    }
    
    func get_user(){
        /*
        if let sessionObj:AnyObject = userDefaults.objectForKey("User"){
            let data = sessionObj as! NSData
            let json = NSKeyedUnarchiver.unarchiveObjectWithData(data)
            let name = json!["display_name"] as? String
            if (self.token != nil){
                self.user_name!.setText(name!)
                self.Gplaylist.setHidden(false)
                self.Gmusic.setHidden(false)
                self.Gsearch.setHidden(false)
            }else{
                self.user_name!.setText("Inicie sesion en el iPhone")
            }
        }else{
        */
        
            let sessionApi = NSURLSession.sharedSession()
            let muableRequest = NSMutableURLRequest(URL: url!)
            muableRequest.setValue("application/json", forHTTPHeaderField: "Accept")
            muableRequest.setValue("Bearer \(self.token)", forHTTPHeaderField: "Authorization")
            muableRequest.HTTPMethod = "GET"
            
            let task = sessionApi.dataTaskWithRequest(muableRequest, completionHandler: { (data, response, error) -> Void in
                if (error != nil){
                    print(error?.localizedDescription)
                    return
                }
                do {
                    let nsdata:NSData = try NSData(data:data!)
                    let json = try NSJSONSerialization.JSONObjectWithData(nsdata, options: .AllowFragments)
                    let sessionData = NSKeyedArchiver.archivedDataWithRootObject(json)
                    self.userDefaults.setObject(sessionData, forKey: "User")
                    self.userDefaults.synchronize()
                    let name = json["display_name"] as? String
                    if (name != nil){
                        self.user_name!.setText(name!)
                        self.Gsearch.setHidden(false)
                        self.Gmusic.setHidden(false)
                        self.Gplaylist.setHidden(false)
                    }else{
                        self.user_name!.setText("Inicie sesion en el iPhone")
                    }
                    
                } catch {
                    self.user_name!.setText("Inicie sesion en el iPhone")
                }
            })
            task.resume()

        
        //}
    }
    
    /*
    @IBAction func next() {
        let messageToSend = ["next":"next"]
        self.session.sendMessage(messageToSend, replyHandler: { (respuesta:[String : AnyObject]) -> Void in
            }) { (error:NSError!) -> Void in
                print(error.localizedDescription)
        }
        
    }
    @IBAction func prev() {
        let messageToSend = ["prev":"prev"]
        self.session.sendMessage(messageToSend, replyHandler: { (respuesta:[String : AnyObject]) -> Void in
            }) { (error:NSError!) -> Void in
                print(error.localizedDescription)
        }

        
    }
 */
    
    

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
