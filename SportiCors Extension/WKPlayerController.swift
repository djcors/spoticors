//
//  PlayerController.swift
//  SpotiCors
//
//  Created by Jonathan on 2/03/16.
//  Copyright © 2016 Jonathan. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class WKPlayerController: WKInterfaceController, WCSessionDelegate {

    @IBOutlet var prev: WKInterfaceButton!
    @IBOutlet var next: WKInterfaceButton!
    @IBOutlet var play: WKInterfaceButton!
    @IBOutlet var trackname: WKInterfaceLabel!
    var toggleState = 1
    @IBOutlet var addremoveicon: WKInterfaceButton!
    
    let myDelegate = WKExtension.sharedExtension().delegate as! ExtensionDelegate
    var session : WCSession!
    var track:Dictionary<String,AnyObject>!
    var index:Int!
    var id:String!
    var check:Bool! = false
    var track_name:String!
    var token:String!

    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        self.token = self.myDelegate.token
        self.setupWatchConnectivity()
        self.track = context!["track"] as! Dictionary<String,AnyObject>!
        self.index = context!["index"] as! Int
        setTitle("Now Playing")
        self.track_name = self.track["name"] as! String
        self.addremoveicon.setHidden(true)
        session.sendMessage(context as! [String : AnyObject], replyHandler: { (respuesta:[String : AnyObject]) -> Void in
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                let title = respuesta["track"]! as! String
                dispatch_async(dispatch_get_main_queue(), {
                   self.trackname.setText(title)
                })
            })
        }) { (error:NSError!) -> Void in
                print(error.localizedDescription)
        }

    }
    
    private func setupWatchConnectivity() {
        if WCSession.isSupported() {
            session = WCSession.defaultSession()
            session.delegate = self
            session.activateSession()
        }
    }
    
    func session(session: WCSession, didReceiveMessage message: [String : AnyObject], replyHandler: ([String : AnyObject]) -> Void) {
        if (message["track"] != nil){
            self.trackname.setText((message["track"] as! String))
            self.index = message["index"] as! Int
            self.track["id"] = message["id"] as! String
            self.checkTrack()
        }
        else if (message["pause"] != nil) {
            self.play.setBackgroundImageNamed("Wpause")
        }
        else if (message["play"] != nil) {
            self.play.setBackgroundImageNamed("Wplay")
        }

    }
    

    override func willActivate() {
        super.willActivate()
        self.trackname.setText((self.track["name"] as! String))
        self.checkTrack()
    }
    
    func session(session: WCSession, didReceiveApplicationContext applicationContext: [String : AnyObject]) {
        
        if applicationContext["track"] != nil{
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                self.track_name = applicationContext["track"] as! String
                self.index = applicationContext["index"] as! Int
                dispatch_async(dispatch_get_main_queue(), {
                    self.trackname.setText(self.track_name)
                })
                print(self.track_name)
            })
        }
    }

    
    
    @IBAction func PlayPause() {
        let messageToSend = ["play":"play"]
        self.session.sendMessage(messageToSend, replyHandler: { (respuesta:[String : AnyObject]) -> Void in
            }) { (error:NSError!) -> Void in
                print(error.localizedDescription)
        }
        if toggleState == 1 {
            self.play.setBackgroundImageNamed("Wplay")
            toggleState = 2
        } else {
            self.play.setBackgroundImageNamed("Wpause")
            toggleState = 1
        }
        
    }

    @IBAction func PrevSong() {
        let messageToSend = ["prev":"prev", "index": self.index ]
        self.session.sendMessage(messageToSend as! [String : AnyObject], replyHandler: { (respuesta:[String : AnyObject]) -> Void in
            self.index = respuesta["index"] as! Int
            self.trackname.setText((respuesta["track"] as! String))
            }) { (error:NSError!) -> Void in
                print(error.localizedDescription)
        }
 
        
    }
    @IBAction func NextSong() {
        let messageToSend = ["next":"next", "index": self.index ]
        self.session.sendMessage(messageToSend as! [String : AnyObject], replyHandler: { (respuesta:[String : AnyObject]) -> Void in
            self.index = respuesta["index"] as! Int
            self.trackname.setText((respuesta["track"] as! String))
            }) { (error:NSError!) -> Void in
                print(error.localizedDescription)
        }
        

    }
    
    func checkTrack(){
        let url = "https://api.spotify.com/v1/me/tracks/contains?ids="
        let param:String! = self.track["id"] as! String
        let completeURL = url + param
        let Url = NSURL(string:completeURL)
        
        let sessionApi = NSURLSession.sharedSession()
        let muableRequest = NSMutableURLRequest(URL: Url!)
        muableRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        muableRequest.setValue("Bearer \(self.token)", forHTTPHeaderField: "Authorization")
        muableRequest.HTTPMethod = "GET"
        
        let task = sessionApi.dataTaskWithRequest(muableRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil){
                print(error?.localizedDescription)
                return
            }
            let nsdata:NSData =  NSData(data:data!)
            do {
                let json = try NSJSONSerialization.JSONObjectWithData(nsdata, options: .AllowFragments)
                if json[0] as! NSObject == 0{
                    self.check = false
                    self.addremoveicon.setBackgroundImageNamed("Wadd")
                }else{
                    self.check = true
                    self.addremoveicon.setBackgroundImageNamed("Wcheck")
                }
                self.addremoveicon.setHidden(false)
            } catch {
                print(error)
            }
        })
        task.resume()

    
    }
    
    
    @IBAction func addremove() {
        let url = "https://api.spotify.com/v1/me/tracks?ids="
        let param:String! = self.track["id"] as! String
        let completeURL = url + param
        let Url = NSURL(string:completeURL)
        let sessionApi = NSURLSession.sharedSession()
        let muableRequest = NSMutableURLRequest(URL: Url!)
        muableRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        muableRequest.setValue("Bearer \(self.token)", forHTTPHeaderField: "Authorization")
        
        if self.check == true{
            muableRequest.HTTPMethod = "DELETE"
        }else{
            muableRequest.HTTPMethod = "PUT"
        }
        let task = sessionApi.dataTaskWithRequest(muableRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil){
                print(error?.localizedDescription)
                return
            }
            self.checkTrack()
            let action2 = WKAlertAction(title: "Ok", style: .Default) {}
            if muableRequest.HTTPMethod == "DELETE" {
                self.presentAlertControllerWithTitle("Deleted", message: "", preferredStyle: .ActionSheet, actions: [action2])
            }else if muableRequest.HTTPMethod == "PUT"{
                self.presentAlertControllerWithTitle("Added", message: "", preferredStyle: .ActionSheet, actions: [action2])
            }
        })
        task.resume()

    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        self.trackname.setText(self.track_name)
        super.didDeactivate()
    }

}
