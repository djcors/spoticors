//
//  ExtensionDelegate.swift
//  SportiCors Extension
//
//  Created by Jonathan on 22/02/16.
//  Copyright © 2016 Jonathan. All rights reserved.
//

import WatchKit
import WatchConnectivity

class ExtensionDelegate: NSObject, WKExtensionDelegate, WCSessionDelegate {
    var session : WCSession!
    var token:String!
    
    let userDefaults = NSUserDefaults.standardUserDefaults()
    
    private func setupWatchConnectivity() {
        if WCSession.isSupported() {
            session = WCSession.defaultSession()
            session.delegate = self
            session.activateSession()
        }
    }

    func applicationDidFinishLaunching() {
        setupWatchConnectivity()
        let messageToSend = ["lat":["hola"]]
        self.session.sendMessage(messageToSend, replyHandler: { (respuesta:[String : AnyObject]) -> Void in
            self.token = respuesta["token"] as! String
            let sessionData = NSKeyedArchiver.archivedDataWithRootObject(self.token)
            self.userDefaults.setObject(sessionData, forKey: "token")
            self.userDefaults.synchronize()
            }) { (error:NSError!) -> Void in
                print(error.localizedDescription)
        }

    }
    
    
    func session(session: WCSession, didReceiveUserInfo userInfo: [String : AnyObject]){
        if (userInfo["token"] != nil){
            self.token = userInfo["token"] as! String
        }
    }
    
    func session(session: WCSession, didReceiveMessage message: [String : AnyObject], replyHandler: ([String : AnyObject]) -> Void) {
        print("mensaje recibido del iphone \(message)")
    }
    
    func session(session: WCSession, didReceiveApplicationContext applicationContext: [String : AnyObject]) {
        if applicationContext["token"] != nil{
            self.token = applicationContext["token"] as! String
            let sessionData = NSKeyedArchiver.archivedDataWithRootObject(self.token)
            self.userDefaults.setObject(sessionData, forKey: "token")
            self.userDefaults.synchronize()
        }
    }

    



    func applicationDidBecomeActive() {
        
        
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    

    func applicationWillResignActive() {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, etc.
    }

}


