//
//  WKPlaylistController.swift
//  SpotiCors
//
//  Created by Jonathan on 26/02/16.
//  Copyright © 2016 Jonathan. All rights reserved.
//

import WatchKit
import Foundation


class WKPlaylistController: WKInterfaceController {
    var token:String!
    let myDelegate = WKExtension.sharedExtension().delegate as! ExtensionDelegate
    let url = NSURL(string: "https://api.spotify.com/v1/me/playlists?limit=50")
    var items:[[String: AnyObject]]!
    var item_select:Int! = 0
    
    @IBOutlet var PlaylistTable: WKInterfaceTable!
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        setTitle("Playlists")
        self.token = self.myDelegate.token
        self.requestMe(self.token)
    }

    override func willActivate() {
        super.willActivate()
    }
    
    func requestMe(token:String!){
        let sessionApi = NSURLSession.sharedSession()
        let muableRequest = NSMutableURLRequest(URL: url!)
        muableRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        muableRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        muableRequest.HTTPMethod = "GET"
        
        let task = sessionApi.dataTaskWithRequest(muableRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil){
                print(error?.localizedDescription)
                return
            }
            let nsdata:NSData =  NSData(data:data!)
            do {
                let json = try NSJSONSerialization.JSONObjectWithData(nsdata, options: .AllowFragments)
                let total:Int = json["total"] as! Int
                self.PlaylistTable.setNumberOfRows(total, withRowType: "cell")
                self.items = json["items"] as! [[String: AnyObject]]
                for (i, item) in self.items.enumerate() {
                    let cell = self.PlaylistTable.rowControllerAtIndex(i) as! PlayListsRow
                    cell.PlayListTitle.setText(String(item["name"]!))
                }
            } catch {
                print(error)
            }
        })
        task.resume()
    }
    
    
    override func table(table: WKInterfaceTable, didSelectRowAtIndex rowIndex: Int) {
        self.item_select = rowIndex
    }
    
    override func contextForSegueWithIdentifier(segueIdentifier: String, inTable table: WKInterfaceTable, rowIndex: Int) -> AnyObject? {
        //self.pushControllerWithName("WKTrackListController", context: self.items[rowIndex])
        return self.items[rowIndex]
    }

    override func didDeactivate() {
        super.didDeactivate()
    }

}
