//
//  SearchController.swift
//  SpotiCors
//
//  Created by Jonathan on 6/03/16.
//  Copyright © 2016 Jonathan. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class SearchController: WKInterfaceController, WCSessionDelegate {
    
    @IBOutlet var Textlabel: WKInterfaceLabel!
    var textChoices = ["Tiesto","Armin Van Buuren"]
    
    
    @IBOutlet var resutlsTable: WKInterfaceTable!
    @IBOutlet var trackstable: WKInterfaceTable!
    
    let url:String! = "https://api.spotify.com/v1/search"
    let userDefaults = NSUserDefaults.standardUserDefaults()
    var market:String!
    var token:String!
    let myDelegate = WKExtension.sharedExtension().delegate as! ExtensionDelegate
    var uris_string = [String]()
    var session : WCSession!
    var track:[[String: AnyObject]]!

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        setTitle("Search")
        session = self.myDelegate.session
        let messageToSend = ["lat":["hola"]]
        session.sendMessage(messageToSend, replyHandler: { (respuesta:[String : AnyObject]) -> Void in
            self.token = respuesta["token"] as! String
            self.myDelegate.token = self.token
            }) { (error:NSError!) -> Void in
                print(error.localizedDescription)
        }
        if let sessionObj:AnyObject = userDefaults.objectForKey("User"){
            let data = sessionObj as! NSData
            let json = NSKeyedUnarchiver.unarchiveObjectWithData(data)
            self.market = json!["market"] as? String
        }
        if let options:AnyObject = userDefaults.objectForKey("resent_search"){
            let data = options as! NSData
            let json = NSKeyedUnarchiver.unarchiveObjectWithData(data) as! [String]
            self.textChoices.removeAll()
            let a = Array(Set(json))
            self.textChoices = a
        }
    }
    
    func Busqueda(){
        if let options:AnyObject = userDefaults.objectForKey("resent_search"){
            let data = options as! NSData
            let json = NSKeyedUnarchiver.unarchiveObjectWithData(data) as! [String]
            self.textChoices.removeAll()
            let a = Array(Set(json))
            self.textChoices = a
        }

        presentTextInputControllerWithSuggestions(self.textChoices,
            allowedInputMode: WKTextInputMode.Plain,
            completion: {(results) -> Void in
                if results != nil && results!.count > 0 { //selection made
                    let aResult = results?[0] as? String
                    let searchtext = aResult!.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)
                    let fullUrl = NSURL(string:"\(self.url!)?q=\(searchtext!)&type=track&limit=20")
                    self.Search(fullUrl!)
                    self.Textlabel.setText(aResult)
                    self.textChoices.append(aResult!)
                    if self.textChoices.count > 10 {
                        self.textChoices.removeAtIndex(0)
                    }
                    let sessionData = NSKeyedArchiver.archivedDataWithRootObject(self.textChoices)
                    self.userDefaults.setObject(sessionData, forKey:"resent_search")
                    self.userDefaults.synchronize()
                }
            }
        )

    
    }
    
    
    func Search(url:NSURL){
        let sessionApi = NSURLSession.sharedSession()
        let muableRequest = NSMutableURLRequest(URL: url)
        muableRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        muableRequest.setValue("Bearer \(self.token)", forHTTPHeaderField: "Authorization")
        muableRequest.HTTPMethod = "GET"
        
        let task = sessionApi.dataTaskWithRequest(muableRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil){
                print(error!.localizedDescription)
                return
            }
            let nsdata:NSData =  NSData(data:data!)
            do {
                let json = try NSJSONSerialization.JSONObjectWithData(nsdata, options: .AllowFragments)
                let tracks = json["tracks"] as! Dictionary<String,AnyObject>
                //let artists = json["artists"] as! Dictionary<String,AnyObject>
                //self.resutlsTable.setNumberOfRows(artists["items"]!.count,withRowType: "Artist")
                self.track = tracks["items"]! as! [[String : AnyObject]]
                self.trackstable.setNumberOfRows(tracks["items"]!.count,withRowType: "Track")
                
                /*
                for (var i = 0; i < artists["items"]!.count; i++) {
                    let artist = artists["items"]![i]! as! Dictionary<String,AnyObject>
                    let cell = self.resutlsTable.rowControllerAtIndex(i) as! TrackListRow
                    cell.trackname.setText(String(artist["name"]!))
                }
                */
                self.uris_string.removeAll()
                for (var i = 0; i < tracks["items"]!.count; i++) {
                    let track = tracks["items"]![i]! as! Dictionary<String,AnyObject>
                    let name = tracks["items"]![i]!["name"]! as! String
                    let cell = self.trackstable.rowControllerAtIndex(i) as! TrackListRow
                    cell.trackname.setText(name)
                    self.uris_string.append(track["uri"]! as! String)
                }


                
            } catch {
                print(error)
            }


        })
        task.resume()
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        self.Busqueda()
        
    }
    
    override func contextForSegueWithIdentifier(segueIdentifier: String, inTable table: WKInterfaceTable, rowIndex: Int) -> AnyObject? {
        //self.pushControllerWithName("WKTrackListController", context: self.items[rowIndex])
        
        let context = ["uris":self.uris_string, "index":rowIndex,"track":self.track[rowIndex] ] as Dictionary<String,AnyObject>
        
        return context
    }


    @IBAction func search() {
        self.Busqueda()
    }
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}