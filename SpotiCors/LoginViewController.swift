//
//  ViewController.swift
//  SpotiCors
//
//  Created by Jonathan on 6/02/16.
//  Copyright © 2016 Jonathan. All rights reserved.
//

import UIKit
import SafariServices

class LoginViewController: UIViewController, SFSafariViewControllerDelegate, SPTAuthViewDelegate{
    
    var window: UIWindow?
    
    let ClientID = "31bea23d765749e685525bbe72aa2fa2"
    let CallbackURL = "spoticors://"
    let TokenSwapURL = "https://spoticors.herokuapp.com/swap"
    let TokenRefreshURL = "https://spoticors.herokuapp.com/refresh"
    var session:SPTSession!
    let userDefaults = NSUserDefaults.standardUserDefaults()
    let auth = SPTAuth.defaultInstance()
    var safari = SFSafariViewController!()
    
    @IBOutlet weak var login_btn: UIButton!
    @IBOutlet weak var refreshtxt: UILabel!
    
    @IBOutlet weak var spiner: UIActivityIndicatorView!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("login_succes"), name: "loginSuccessful", object: nil)
        if let sessionObj:AnyObject = userDefaults.objectForKey("SpotifySession"){
            let data = sessionObj as! NSData
            self.session = NSKeyedUnarchiver.unarchiveObjectWithData(data) as! SPTSession
        }
        
        if self.session != nil{
            self.refreshtxt.hidden = false
            self.login_btn.hidden = true
            self.view.backgroundColor = UIColor.blackColor()
            self.refreshtxt.textColor = UIColor.whiteColor()
            self.spiner.hidden = false
        }else{
            self.refreshtxt.hidden = true
            self.login_btn.hidden = false
            self.spiner.hidden = true
        }
    }

    @IBAction func LoginButton(sender: AnyObject) {
        let scopes = ["playlist-read-private", "playlist-read-collaborative", "playlist-modify-public", "playlist-modify-private", "streaming", "user-library-read", "user-library-modify", "user-read-private", "user-follow-modify", "user-follow-read", "user-read-birthdate", "user-read-email"]
        
        
        auth.clientID = ClientID
        auth.redirectURL = NSURL(string: CallbackURL)
        auth.tokenSwapURL = NSURL(string: TokenSwapURL)
        auth.tokenRefreshURL = NSURL(string: TokenRefreshURL)
        auth.requestedScopes = scopes
        auth.sessionUserDefaultsKey = "SpotifySession"
        
        let spotifyAuthenticationViewController = SPTAuthViewController.authenticationViewControllerWithAuth(auth)
        spotifyAuthenticationViewController.delegate = self
        spotifyAuthenticationViewController.modalPresentationStyle = UIModalPresentationStyle.OverCurrentContext
        
        spotifyAuthenticationViewController.definesPresentationContext = true
        presentViewController(spotifyAuthenticationViewController, animated: false, completion: nil)
        

        
        
        //let loginURL = auth.loginURL
        //let safariVC = SFSafariViewController(URL: loginURL)
        //self.presentViewController(safariVC, animated: true, completion: nil)
        //self.safari = safariVC
    }
    
    func safariViewControllerDidFinish(controller: SFSafariViewController){
        //controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func authenticationViewController(authenticationViewController: SPTAuthViewController!, didLoginWithSession session: SPTSession!) {
    
        login_succes()
    }
    
    func authenticationViewController(authenticationViewController: SPTAuthViewController!, didFailToLogin error: NSError!) {
        print("login failed")
    }
    
    func authenticationViewControllerDidCancelLogin(authenticationViewController: SPTAuthViewController!) {
        print("login cancelled")
    }
    

    
    func login_succes(){
        //safariViewControllerDidFinish(self.safari)
        let appDelegate = UIApplication.sharedApplication().delegate! as! AppDelegate
        let initialViewController = self.storyboard!.instantiateViewControllerWithIdentifier("SWRevealViewController")
        appDelegate.window?.rootViewController = initialViewController
        appDelegate.window?.makeKeyAndVisible()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

