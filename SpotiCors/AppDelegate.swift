//
//  AppDelegate.swift
//  SpotiCors
//
//  Created by Jonathan on 6/02/16.
//  Copyright © 2016 Jonathan. All rights reserved.
//

import UIKit
import AVFoundation
import WatchConnectivity
import MediaPlayer


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, SPTAudioStreamingDelegate, SPTAudioStreamingPlaybackDelegate, WCSessionDelegate {

    var window: UIWindow?
    
    let ClientID = "31bea23d765749e685525bbe72aa2fa2"
    let CallbackURL = "spoticors://"
    let TokenSwapURL = "https://spoticors.herokuapp.com/swap"
    let TokenRefreshURL = "https://spoticors.herokuapp.com/refresh"
    var trackPlayer: SPTAudioStreamingController?
    var session:SPTSession!
    var SessionUserDefaultsKey = "SpotifySession"
    let auth = SPTAuth.defaultInstance()
    var player:SPTAudioStreamingController?

    var user:SPTUser!
    var Wsession : WCSession!
    
    let audioSession = AVAudioSession.sharedInstance()
    let userDefaults = NSUserDefaults.standardUserDefaults()
    
    let commandCenter = MPRemoteCommandCenter.sharedCommandCenter()
    var cover: UIImage!
    let mpic = MPNowPlayingInfoCenter.defaultCenter()
    var partial = [String:AnyObject]()
    
    let scopes = ["playlist-read-private", "playlist-read-collaborative", "playlist-modify-public", "playlist-modify-private", "streaming", "user-library-read", "user-library-modify", "user-read-private", "user-follow-modify", "user-follow-read", "user-read-birthdate", "user-read-email"]


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        setupWatchConnectivity()
        let URLCache = NSURLCache(memoryCapacity: 4 * 1024 * 1024, diskCapacity: 20 * 1024 * 1024, diskPath: nil)
        NSURLCache.setSharedURLCache(URLCache)
        
        UIApplication.sharedApplication().beginReceivingRemoteControlEvents()
        self.commandCenter.nextTrackCommand.enabled = true
        self.commandCenter.nextTrackCommand.addTarget(self, action: Selector("next"))
        self.commandCenter.previousTrackCommand.enabled = true
        self.commandCenter.previousTrackCommand.addTarget(self, action: Selector("prev"))
        self.commandCenter.pauseCommand.enabled = true
        self.commandCenter.pauseCommand.addTarget(self, action: Selector("play"))
        self.commandCenter.playCommand.enabled = true
        self.commandCenter.playCommand.addTarget(self, action: Selector("play"))

        
        if let sessionObj:AnyObject = userDefaults.objectForKey("SpotifySession"){
            let data = sessionObj as! NSData
            self.session = NSKeyedUnarchiver.unarchiveObjectWithData(data) as! SPTSession
        }
        
        if self.session != nil{
            if self.session.isValid(){
                self.getUser()
                self.LoadPlayer()
                self.startrefresh()
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let rootController = storyboard.instantiateViewControllerWithIdentifier("SWRevealViewController")
                do {
                    let token = ["token": self.session.accessToken]
                    try WCSession.defaultSession().updateApplicationContext(token)
                }
                catch {
                    print("error actualizando el context")
                }
                if let window = self.window {
                    window.rootViewController = rootController
                }
                
                do {
                    let token = ["token": self.session.accessToken]
                    try WCSession.defaultSession().transferUserInfo(token)
                }
                catch {
                    print("error")
                }
            }else{
                self.refresh()
            }
        }
        return true
    }
    
    
    
    
    func session(session: WCSession, didReceiveMessage message: [String : AnyObject], replyHandler: ([String : AnyObject]) -> Void) {
        if self.session != nil{
            
            if (message["lat"] != nil){
                let token = ["token": self.session.accessToken]
                replyHandler(token)
            }
            else if (message["uris"] != nil){
                let uris_string = message["uris"]! as! NSArray
                let index = Int(message["index"]! as! NSNumber)
                var track:SPTPartialTrack!
                do {
                    track = try SPTPartialTrack.init(fromDecodedJSON: message["track"]!)
                } catch {
                    print(error)
                }
                self.sendToPlayer(uris_string, index: index, track: track)
            }
            else if (message["play"] != nil){
                self.play()
            }
            else if (message["prev"] != nil){
                self.prev()
            }
            else if (message["next"] != nil){
                self.next()
            }
            else if (message["renew"] != nil){
                self.refresh()
                let token = ["token": self.session.accessToken]
                replyHandler(token)
            }
        }
    }
    
    func sendToPlayer(Uris:NSArray, index:Int, track:SPTPartialTrack){
        var uris = [NSURL]()
        for(var i = 0; i < Uris.count; i++){
            let uri = NSURL(string: Uris[i] as! String)
            uris.append(uri!)
        }
        let options = SPTPlayOptions()
        options.trackIndex = Int32(index)
        options.startTime = 0

        self.LoadPlayer()
        self.player?.playURIs(uris, withOptions: options, callback: { (error:NSError!) -> Void in
            if(error != nil){
                print("player \(error.localizedDescription)")
                return
            }
                    })
    }
    
    
    // refresh login
    func startrefresh(){
        _ = NSTimer.scheduledTimerWithTimeInterval(3500, target: self, selector: #selector(AppDelegate.refresh), userInfo: nil, repeats: true )
    }
    
    func refresh(){
        self.auth.clientID = ClientID
        self.auth.redirectURL = NSURL(string: CallbackURL)
        self.auth.tokenSwapURL = NSURL(string: TokenSwapURL)
        self.auth.tokenRefreshURL = NSURL(string: TokenRefreshURL)
        self.auth.requestedScopes = scopes
        
        
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            self.auth.renewSession(self.session, callback: { (error:NSError!, session:SPTSession!) -> Void in
                if (error != nil){
                    print("error renew session \(error.code)  \(error.localizedDescription)")
                    return
                }
                self.session = session
                let userDefaults = NSUserDefaults.standardUserDefaults()
                let sessionData = NSKeyedArchiver.archivedDataWithRootObject(session)
                userDefaults.setObject(sessionData, forKey: self.SessionUserDefaultsKey)
                userDefaults.synchronize()
                self.getUser()
                self.LoadPlayer()
                self.auth.sessionUserDefaultsKey = self.SessionUserDefaultsKey
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let rootController = storyboard.instantiateViewControllerWithIdentifier("SWRevealViewController")
                do {
                    let token = ["token": self.session.accessToken]
                    try WCSession.defaultSession().updateApplicationContext(token)
                }
                catch {
                    print("error actualizando el context")
                }
                if let window = self.window {
                    window.rootViewController = rootController
                }

                do {
                    let token = ["token": self.session.accessToken]
                    try WCSession.defaultSession().transferUserInfo(token)
                }
                catch {
                    print("error")
                }
                
            })
        }
    }

    
    
    func LoadPlayer() {
        if self.player == nil{
            self.player = SPTAudioStreamingController(clientId: ClientID)
            self.player?.delegate = self
            self.player?.playbackDelegate = self
            self.player?.diskCache = SPTDiskCache(capacity: 1024 * 1024 * 64)
            
        }
        self.player?.loginWithSession(self.session, callback: { (error:NSError!) -> Void in
            if (error != nil){
                print("error session player \(error.localizedDescription) ")
                return
            }
            
            try! self.audioSession.setCategory("AVAudioSessionCategoryPlayback")
            try! self.audioSession.setActive(true)
        })
    }
    
    func getUser(){
        SPTUser.requestCurrentUserWithAccessToken(self.session.accessToken) { (error:NSError!, user:AnyObject!) -> Void in
            if (error != nil){
                print(error.localizedDescription)
                return
            }
            self.user = user as! SPTUser
        }
    }
    
    func application(app: UIApplication, openURL url: NSURL, options: [String : AnyObject]) -> Bool {
        if SPTAuth.defaultInstance().canHandleURL(NSURL(string:CallbackURL)){
             SPTAuth.defaultInstance().handleAuthCallbackWithTriggeredAuthURL(url, callback: { (error: NSError!, session: SPTSession!) -> Void in
                if error != nil{
                    print(error.localizedDescription)
                    return
                }
                self.session = session
                let userDefaults = NSUserDefaults.standardUserDefaults()
                let sessionData = NSKeyedArchiver.archivedDataWithRootObject(session)
                userDefaults.setObject(sessionData, forKey: self.SessionUserDefaultsKey)
                userDefaults.synchronize()
                NSNotificationCenter.defaultCenter().postNotificationName("loginSuccessful", object: nil)
                self.auth.sessionUserDefaultsKey = self.SessionUserDefaultsKey
                self.LoadPlayer()
                self.getUser()
                self.startrefresh()
                do {
                    let token = ["token": self.session.accessToken]
                    try WCSession.defaultSession().transferUserInfo(token)
                }
                catch {
                    print("error transferUserInfo")
                }

            })
        }
        return false
    }
    
    private func setupWatchConnectivity() {
        if WCSession.isSupported() {
            Wsession = WCSession.defaultSession()
            Wsession.delegate = self
            Wsession.activateSession()
        }
    }
    
    
    //Control center & LoockScreen
    func UpdateCover(album:SPTImage){
        if (album.imageURL != nil){
            let data = NSData(contentsOfURL: album.imageURL)
            let image = UIImage(data: data!)!
            self.cover = image
            let atwork = MPMediaItemArtwork(image: image)
            dispatch_async(dispatch_get_main_queue(), {
                self.mpic.nowPlayingInfo?.updateValue(atwork, forKey: MPMediaItemPropertyArtwork)
            })
        }
    }
    
    func MPplayer(){
        let atwork = MPMediaItemArtwork(image: self.cover)
        let duration = self.player!.currentTrackDuration
        _ = MPMediaType.Music
        dispatch_async(dispatch_get_main_queue(), {
            self.mpic.nowPlayingInfo = [
                MPMediaItemPropertyTitle:self.player?.currentTrackMetadata["SPTAudioStreamingMetadataTrackName"] as! String,
                MPMediaItemPropertyArtist:self.player?.currentTrackMetadata["SPTAudioStreamingMetadataArtistName"] as! String,
                MPMediaItemPropertyArtwork:atwork,
                MPMediaItemPropertyPlaybackDuration: duration,
            ]
        })
    }
    
    func updateAbumCover(uri:String){
        SPTAlbum.albumWithURI(NSURL(string: uri), session: self.session, callback:  { (error:NSError!, album:AnyObject!) -> Void in
            if (error != nil){
                print(error.localizedDescription)
                return
            }
            self.UpdateCover(album.largestCover)
        })
    }
    
    func play(){
        if (self.player!.isPlaying == true){
            self.player?.setIsPlaying(false, callback: { (error:NSError!) -> Void in
                if (error != nil){
                    print(error.localizedDescription)
                    return
                }
                let context = ["play":"pause"]
                if self.Wsession.reachable {
                    self.Wsession.sendMessage(context, replyHandler: { (respuesta:[String : AnyObject]) -> Void in
                        }) { (error:NSError!) -> Void in
                            print("play \(error.code) - \(error.localizedDescription)")
                    }
                }
            })
        }else{
            self.player?.setIsPlaying(true, callback: { (error:NSError!) -> Void in
                if (error != nil){
                    print(error.localizedDescription)
                    return
                }
                let context = ["pause":"play"]
                if self.Wsession.reachable {
                    self.Wsession.sendMessage(context, replyHandler: { (respuesta:[String : AnyObject]) -> Void in
                        }) { (error:NSError!) -> Void in
                            print("play \(error.code) - \(error.localizedDescription)")
                    }
                }

            })
            
        }
        
    }
    
    func prev(){
        self.player?.skipPrevious({ (error:NSError!) -> Void in
        })
    }
    
    
    func next(){
        self.player?.skipNext({ (error:NSError!) -> Void in
        })
    }



    
    
    
    func applicationWillResignActive(application: UIApplication) {
        if self.session != nil{
            if  !self.session.isValid(){
                self.startrefresh()
            }
        }
    }

    func applicationDidEnterBackground(application: UIApplication) {
        setupWatchConnectivity()
        if self.session != nil && self.session.isValid(){
            self.startrefresh()
        }
    }

    

    func applicationWillEnterForeground(application: UIApplication) {
        setupWatchConnectivity()
        if self.session != nil && self.session.isValid(){
            self.startrefresh()
        }
    }

    func applicationDidBecomeActive(application: UIApplication) {
        setupWatchConnectivity()
        if self.session != nil && self.session.isValid(){
            self.startrefresh()
        }
    }

    func applicationWillTerminate(application: UIApplication) {
        setupWatchConnectivity()
        if self.session != nil && self.session.isValid(){
            self.startrefresh()
        }
    }
        
    func audioStreaming(audioStreaming: SPTAudioStreamingController!, didStartPlayingTrack trackUri: NSURL!){
        SPTTrack.trackWithURI(trackUri, session: self.session) { (error, track) -> Void in
            if (error != nil){
                print("error audioStreaming \(error.localizedDescription)")
                return
            }
            let album = track.album as SPTPartialAlbum
            self.UpdateCover(album.largestCover)
            self.MPplayer()
            let cancion = track as! SPTPartialTrack
            self.partial = ["track":cancion]
            NSNotificationCenter.defaultCenter().postNotificationName("TrackChange", object: nil, userInfo: self.partial)
            
            let index = Int((self.player?.currentTrackIndex)!)
            let context = ["track":cancion.name, "index":index, "id":cancion.identifier] as Dictionary<String,AnyObject>
            if self.Wsession.reachable {
                self.Wsession.sendMessage(context, replyHandler: { (respuesta:[String : AnyObject]) -> Void in
                    }) { (error:NSError!) -> Void in
                        print("audioStreaming \(error.code) - \(error.localizedDescription)")
                }
            }
            do {
                try WCSession.defaultSession().updateApplicationContext(context)
                print("delegate contexto enviado")
            }
            catch {
                print("error")
            }
        }
    }
}