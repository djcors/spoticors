//
//  MyMusicViewController.swift
//  SpotiCors
//
//  Created by Jonathan on 5/03/16.
//  Copyright © 2016 Jonathan. All rights reserved.
//

import UIKit

class MyMusicViewController: UITableViewController{
    
    var session:SPTSession!
    var total:Int! = 0
    var total_tracks:Int!
    var image:UIImage!
    var uris = [NSURL]()
    var arrary_tracks = [SPTSavedTrack]()
    var canciones:SPTPlaylistSnapshot?
    var item_select:Int = 0
    let userDefaults = NSUserDefaults.standardUserDefaults()
    var pag:SPTListPage!

    @IBOutlet var Music: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "login_succes", name: "loginSuccessful", object: session)
        if let sessionObj:AnyObject = userDefaults.objectForKey("SpotifySession"){
            let data = sessionObj as! NSData
            self.session = NSKeyedUnarchiver.unarchiveObjectWithData(data) as! SPTSession
        }
        
        self.title = "Mi Música"
        let myBtn: UIButton = UIButton()
        myBtn.setImage(UIImage(named: "menu"), forState: .Normal)
        myBtn.frame = CGRectMake(0, 0, 30, 70)
        myBtn.addTarget(self.revealViewController(), action: "revealToggle:", forControlEvents: .TouchUpInside)
        self.navigationItem.setLeftBarButtonItem(UIBarButtonItem(customView: myBtn), animated: true)
        //gestos
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        self.tableView.tintColor = UIColor.whiteColor()
        self.tableView.backgroundColor = UIColor.blackColor()
        self.view.backgroundColor = UIColor.blackColor()
        self.getTracks()
    }
    
    
    func getTracks(){
        SPTYourMusic.savedTracksForUserWithAccessToken(self.session.accessToken) { (error:NSError!, page:AnyObject!) -> Void in
            if(error != nil){
                print(error.localizedDescription)
                return
            }
            let page = page as! SPTListPage
            if page.totalListLength > 0{
                for(var e = 0; e < page.tracksForPlayback().count; e++){
                    let arr = page.tracksForPlayback()[e] as! SPTSavedTrack
                    self.arrary_tracks.append(arr)
                    self.uris.append(arr.uri)
                }
            }
            
            if page.hasNextPage{
                self.getNewpage(page)
            }else{
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                    self.total = self.arrary_tracks.count
                    dispatch_async(dispatch_get_main_queue()) {
                        self.Music.reloadData()
                    }
                })
            }
        }
        
    }
    
    
    func getNewpage(var actual:SPTListPage){
        actual.requestNextPageWithSession(self.session) { (error:NSError!, page:AnyObject!) -> Void in
            if (error != nil){
                print(error.localizedDescription)
                return
            }
            actual = page as! SPTListPage
            for(var e = 0; e < actual.tracksForPlayback().count; e++){
                let arr = actual.tracksForPlayback()[e] as! SPTSavedTrack
                self.arrary_tracks.append(arr)
                self.uris.append(arr.uri)
            }
            if actual.hasNextPage{
                self.getNewpage(actual)
            }
            else{
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                self.total = self.arrary_tracks.count
                    dispatch_async(dispatch_get_main_queue()) {
                        self.Music.reloadData()
                    }
                })
            }
        }
    }
    
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.total
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = self.Music.dequeueReusableCellWithIdentifier("cell")
        if (cell != nil)
        {
            cell = UITableViewCell(style: UITableViewCellStyle.Subtitle,
                reuseIdentifier: "cell")
        }

            cell!.backgroundColor = UIColor.blackColor()
            cell!.textLabel?.textColor = UIColor.whiteColor()
            
                let partial = self.arrary_tracks[indexPath.row] as SPTPartialTrack
                cell!.textLabel?.text = partial.name
                
                var totalartist = [String]()
                for(var i = 0; i < partial.artists.count; i++){
                    totalartist.append(partial.artists[i].name)
                }
                let stringRepresentation = totalartist.joinWithSeparator(", ")
                cell!.detailTextLabel?.text = stringRepresentation
                cell!.detailTextLabel?.textColor = UIColor.lightGrayColor()
        
        return cell!
    }
    

    
    
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        item_select = indexPath.row
        self.performSegueWithIdentifier("Mplayer", sender: item_select)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "Mplayer") {
            if let PlayerViewController = segue.destinationViewController as? PlayerViewController {
                PlayerViewController.session = self.session
                PlayerViewController.track_s = self.arrary_tracks[item_select]
                PlayerViewController.uris = self.uris
                PlayerViewController.index = Int32(item_select)
            }
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    



}
