//
//  SearchViewController.swift
//  SpotiCors
//
//  Created by Jonathan on 9/02/16.
//  Copyright © 2016 Jonathan. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {
    
    var session:SPTSession!
    var total_canciones:Int! = 1
    var total_artistas:Int! = 1
    var total_albumes:Int! = 1
    var total_playlist:Int! = 1
    var Artistas:SPTListPage?
    var canciones:SPTListPage?
    var albumes:SPTListPage?
    var playlist:SPTListPage?
    var item_select:Int = 0
    var market = SPTMarketFromToken
    let userDefaults = NSUserDefaults.standardUserDefaults()
    var arrary_tracks = [SPTPartialTrack]()
    
    @IBOutlet weak var resutlsTable: UITableView!
    @IBOutlet weak var SearchBar: UISearchBar!
    var searchActive : Bool = false
    let titulos = ["Canciones", "Artistas", "Albumes", "Playlist"]
    var uris = [NSURL]()

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.resutlsTable.allowsSelection = true
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "login_succes", name: "loginSuccessful", object: session)
        if let sessionObj:AnyObject = userDefaults.objectForKey("SpotifySession"){
            let data = sessionObj as! NSData
            self.session = NSKeyedUnarchiver.unarchiveObjectWithData(data) as! SPTSession
        }
        
        let myBtn: UIButton = UIButton()
        myBtn.setImage(UIImage(named: "menu"), forState: .Normal)
        myBtn.frame = CGRectMake(0, 0, 30, 70)
        myBtn.addTarget(self.revealViewController(), action: "revealToggle:", forControlEvents: .TouchUpInside)
        self.navigationItem.setLeftBarButtonItem(UIBarButtonItem(customView: myBtn), animated: true)
            //gestos
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        
        self.resutlsTable.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.resutlsTable.numberOfRowsInSection(0)
        self.resutlsTable.tintColor = UIColor.whiteColor()
        self.resutlsTable.backgroundColor = UIColor.blackColor()
        self.view.backgroundColor = UIColor.blackColor()
    }
    
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchActive = false;
        searchBar.resignFirstResponder()
        searchBar.text = ""
        self.resutlsTable.numberOfRowsInSection(0)
        self.resutlsTable.reloadData()
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchActive = false;
        self.SearchArtists(searchBar.text!)
        self.SearchTracks(searchBar.text!)
        self.SearchAlbumes(searchBar.text!)
        self.SearchPlaylist(searchBar.text!)
        
    }
    
    func SearchArtists(query:String){
        SPTSearch.performSearchWithQuery(query, queryType: SPTSearchQueryType.QueryTypeArtist, offset: 99, accessToken: self.session.accessToken) { (error:NSError!, results:AnyObject!) -> Void in
            if(error != nil){
                print("error en busqueda \(error.localizedDescription)")
                return
            }
            self.Artistas = results as? SPTListPage
            if (self.Artistas?.totalListLength > 0){
                self.total_artistas = self.Artistas!.items.count
                self.dismissKeyboard()
            }
            else{
                self.total_artistas = 0
            }
            self.resutlsTable.reloadData()
        }
    }
    
    func SearchTracks(query:String){
        SPTSearch.performSearchWithQuery(query, queryType: SPTSearchQueryType.QueryTypeTrack, offset: 99, accessToken: self.session.accessToken) { (error:NSError!, results:AnyObject!) -> Void in
            if(error != nil){
                print("error en busqueda \(error.localizedDescription)")
                return
            }
            self.canciones = results as? SPTListPage
            if (self.canciones?.totalListLength > 0){
                self.total_canciones = self.canciones!.items.count
                self.dismissKeyboard()
                self.uris.removeAll()
                self.arrary_tracks.removeAll()
                for(var i = 0; i < self.canciones!.items.count; i++){
                    let arr = self.canciones!.items[i]
                    self.uris.append(arr.uri)
                    self.arrary_tracks.append(arr as! SPTPartialTrack)
                }
            }
            else{
                self.total_canciones = 0
            }
            self.resutlsTable.reloadData()
        }
    }
    
    func SearchAlbumes(query:String){
        SPTSearch.performSearchWithQuery(query, queryType: SPTSearchQueryType.QueryTypeAlbum, offset: 99, accessToken: self.session.accessToken) { (error:NSError!, results:AnyObject!) -> Void in
            if(error != nil){
                print("error en busqueda \(error.localizedDescription)")
                return
            }
            self.albumes = results as? SPTListPage
            if (self.albumes?.totalListLength > 0){
                self.total_albumes = self.albumes!.items.count
                self.dismissKeyboard()
            }
            else{
                self.total_albumes = 0
            }
            self.resutlsTable.reloadData()
        }
    }
    
    func SearchPlaylist(query:String){
        SPTSearch.performSearchWithQuery(query, queryType: SPTSearchQueryType.QueryTypePlaylist, offset: 99, accessToken: self.session.accessToken) { (error:NSError!, results:AnyObject!) -> Void in
            if(error != nil){
                print("error en busqueda \(error.localizedDescription)")
                return
            }
            self.playlist = results as? SPTListPage
            if (self.playlist?.totalListLength > 0){
                self.total_playlist = self.playlist!.items.count
                self.dismissKeyboard()
            }
            else{
                self.total_playlist = 0
            }
            self.resutlsTable.reloadData()
        }
    }

    
    
    
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if let names = self.titulos as [String]?
        {
            return names[section]
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.titulos.count
    }
    
    
    func tableView(tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        if section == 0{
            return self.total_canciones
        }
        else if section == 1{
            return self.total_artistas
        }
        else if section == 2 {
            return self.total_albumes
        }
        else if section == 3 {
            return self.total_playlist
        }
        else {
            return 0
        }
    }
    
    func tableView(tableView:UITableView, cellForRowAtIndexPath indexPath:NSIndexPath) -> UITableViewCell{
        let cell = UITableViewCell(style: UITableViewCellStyle.Subtitle,
            reuseIdentifier: "cell")
        
        cell.backgroundColor = UIColor.blackColor()
        cell.textLabel?.textColor = UIColor.whiteColor()
        cell.detailTextLabel?.textColor = UIColor.whiteColor()
        if(indexPath.section == 0){
            if(self.canciones != nil){
                let track = self.canciones?.items[indexPath.row] as! SPTPartialTrack
                var totalartist = [String]()
                for(var i = 0; i < track.artists.count; i++){
                    totalartist.append(track.artists[i].name)
                }
                let stringRepresentation = totalartist.joinWithSeparator(", ")
                cell.detailTextLabel?.text = stringRepresentation
                cell.textLabel?.text = self.canciones!.items[indexPath.row].name
            }
        }
        
        if(indexPath.section == 1){
            if(self.Artistas != nil){
                cell.textLabel?.text = self.Artistas!.items[indexPath.row].name
            }
        }
        
        if(indexPath.section == 2){
            if(self.albumes != nil){
                cell.textLabel?.text = self.albumes!.items[indexPath.row].name
            }
        }
        
        if(indexPath.section == 3){
            if(self.playlist != nil){
                cell.textLabel?.text = self.playlist!.items[indexPath.row].name
            }
        }
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if(indexPath.section == 0){
            self.item_select = indexPath.row
            self.performSegueWithIdentifier("player2", sender: item_select)
        }

        
        if(indexPath.section == 1){
            self.item_select = indexPath.row
            self.performSegueWithIdentifier("artist", sender: item_select)
        }
        
        if(indexPath.section == 2){
            self.item_select = indexPath.row
            self.performSegueWithIdentifier("album", sender: item_select)
        }
        
        if(indexPath.section == 3){
            self.item_select = indexPath.row
            self.performSegueWithIdentifier("tracklist2", sender: item_select)
        }
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "player2") {
            if let PlayerViewController = segue.destinationViewController as? PlayerViewController {
                PlayerViewController.session = self.session
                PlayerViewController.track_s = self.arrary_tracks[item_select]
                PlayerViewController.uris = self.uris
                PlayerViewController.index = Int32(item_select)
            }
        
        }
        
        if(segue.identifier == "tracklist2") {
            if let TrackListController = segue.destinationViewController as? TrackListController {
                TrackListController.session = self.session
                TrackListController.PartialTracklist = self.playlist!.items[item_select] as! SPTPartialPlaylist
                TrackListController.total = self.playlist!.items[item_select].trackCount.toInt
            }
        }
        
        if(segue.identifier == "album") {
            if let AlbumTableViewController = segue.destinationViewController as? AlbumTableViewController {
                AlbumTableViewController.session = self.session
                AlbumTableViewController.PartialAlbum = self.albumes!.items[item_select] as! SPTPartialAlbum
            }
        }

        
        
        if(segue.identifier == "artist") {
            if let ArtistTableViewController = segue.destinationViewController as? ArtistTableViewController {
                ArtistTableViewController.session = self.session
                ArtistTableViewController.PartialTracklist = self.Artistas!.items[item_select] as! SPTPartialArtist
            }
        }

    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}