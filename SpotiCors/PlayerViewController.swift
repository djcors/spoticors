//
//  PlayerViewController.swift
//  SpotiCors
//
//  Created by Jonathan on 13/02/16.
//  Copyright © 2016 Jonathan. All rights reserved.
//

import UIKit
import MediaPlayer
import AVFoundation
import WatchConnectivity

class PlayerViewController: UIViewController, AVAudioPlayerDelegate, WCSessionDelegate{

    @IBOutlet weak var duracion: UILabel!
    @IBOutlet weak var currentTime: UILabel!
    @IBOutlet weak var trackname: UILabel!
    @IBOutlet weak var imagecover: UIImageView!
    @IBOutlet weak var artistname: UILabel!
    @IBOutlet weak var slider: UISlider!
    
    var session:SPTSession!
    var track:SPTPlaylistTrack!
    var track_s:SPTPartialTrack!
    var artist:String!
    var uris = [NSURL]()
    var options = SPTPlayOptions()
    var index:Int32?
    let appDelegate = UIApplication.sharedApplication().delegate! as! AppDelegate
    
    let commandCenter = MPRemoteCommandCenter.sharedCommandCenter()
    var cover: UIImage!
    let mpic = MPNowPlayingInfoCenter.defaultCenter()
    
    @IBOutlet weak var prevbutton: UIButton!
    @IBOutlet weak var nextbutton: UIButton!
    @IBOutlet weak var playbutton: UIButton!
    var Wsession : WCSession!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupWatchConnectivity()
        self.trackname.textColor = UIColor.whiteColor()
        self.artistname.textColor = UIColor.whiteColor()
        self.duracion.textColor = UIColor.whiteColor()
        self.currentTime.textColor = UIColor.whiteColor()
        self.view.backgroundColor = UIColor.blackColor()
        
        //UI shadows
        self.imagecover.layer.shadowColor = UIColor.blackColor().CGColor
        self.imagecover.layer.shadowOffset = CGSizeMake(0, 2)
        self.imagecover.layer.shadowRadius = 5
        self.imagecover.layer.shadowOpacity = 0.5
        
        
        self.prevbutton.layer.shadowColor = UIColor.blackColor().CGColor
        self.prevbutton.layer.shadowOffset = CGSizeMake(0, 0)
        self.prevbutton.layer.shadowRadius = 5
        self.prevbutton.layer.shadowOpacity = 0.8
        
        
        
        self.nextbutton.layer.shadowColor = UIColor.blackColor().CGColor
        self.nextbutton.layer.shadowOffset = CGSizeMake(0, 0)
        self.nextbutton.layer.shadowRadius = 5
        self.nextbutton.layer.shadowOpacity = 0.8
        
        
        self.playbutton.layer.shadowColor = UIColor.blackColor().CGColor
        self.playbutton.layer.shadowOffset = CGSizeMake(0, 0)
        self.playbutton.layer.shadowRadius = 5
        self.playbutton.layer.shadowOpacity = 0.8
        
        if self.index != nil{
            self.options.trackIndex = self.index!
            self.options.startTime = 0
        }
        if self.uris.count > 0{
            self.startPlayer()
        }
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("changetrack:"), name: "TrackChange", object: nil)
        
    
        
    }
    
    func setupWatchConnectivity() {
        if WCSession.isSupported() {
            Wsession = WCSession.defaultSession()
            Wsession.delegate = self
            Wsession.activateSession()
        }
    }
    
    func session(session: WCSession, didReceiveMessage message: [String : AnyObject], replyHandler: ([String : AnyObject]) -> Void) {
        if (message["lat"] != nil){
            let token = ["token": self.session.accessToken]
            replyHandler(token)
        }
        else if (message["uris"] != nil){
            let uris_string = message["uris"]! as! NSArray
            let index = Int(message["index"]! as! NSNumber)
            do {
                let track = try SPTPartialTrack.init(fromDecodedJSON: message["track"]!)
                self.setTrackInfo_s(track)
                self.track_s = track
            } catch {
                print(error)
            }
            self.uris.removeAll()
            for(var i = 0; i < uris_string.count; i++){
                let uri = NSURL(string: uris_string[i] as! String)
                self.uris.append(uri!)
            }
            self.index = Int32(index)
            
            self.startPlayer()
            self.options.trackIndex = self.index!
            self.options.startTime = 0
            self.MPplayer()
        }
        else if (message["play"] != nil){
            self.play()
        }
        else if (message["prev"] != nil){
            self.prev()
        }
        else if (message["next"] != nil){
            self.next()
        }
    }

    
    
    func setTrackInfo(track:SPTPlaylistTrack){
        var totalartist = [String]()
        for(var i = 0; i < track.artists.count; i++){
            totalartist.append(track.artists[i].name)
        }
        
        self.UpdateCover(track.album.largestCover)
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
            let stringRepresentation = totalartist.joinWithSeparator(", ")
            dispatch_async(dispatch_get_main_queue()) {
                self.trackname.text = track.name
                self.artistname.text = stringRepresentation
            }
        })
    }
    
    func setTrackInfo_s(track_s:SPTPartialTrack){
        var totalartist = [String]()
        for(var i = 0; i < track_s.artists.count; i++){
            totalartist.append(track_s.artists[i].name)
        }
        self.UpdateCover(track_s.album.largestCover)
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
            let stringRepresentation = totalartist.joinWithSeparator(", ")
            dispatch_async(dispatch_get_main_queue()) {
                self.trackname.text = track_s.name
                self.artistname.text = stringRepresentation
            }
        })

    }

    
    func UpdateCover(album:SPTImage){
        if (album.imageURL != nil){
            let data = NSData(contentsOfURL: album.imageURL)
            let image = UIImage(data: data!)!
            self.cover = image
            let atwork = MPMediaItemArtwork(image: image)
            
            let imageView = UIImageView(image: image)
            imageView.frame = self.view.bounds
            imageView.contentMode = .ScaleToFill
            
            
            let blurEffect = UIBlurEffect(style: .Dark)
            let blurredEffectView = UIVisualEffectView(effect: blurEffect)
            blurredEffectView.frame = imageView.bounds
            
            
            dispatch_async(dispatch_get_main_queue(), {
                self.imagecover.image = image
                self.mpic.nowPlayingInfo?.updateValue(atwork, forKey: MPMediaItemPropertyArtwork)
                self.view.insertSubview(imageView, atIndex: 0)
                self.view.insertSubview(blurredEffectView, atIndex: 1)
            })

        }
    }
    

    
    
    func startPlayer(){
        self.appDelegate.player?.playURIs(self.uris, withOptions: self.options, callback: { (error:NSError!) -> Void in
            if(error != nil){
                print("player \(error.localizedDescription)")
                return
            }
            //UIApplication.sharedApplication().beginReceivingRemoteControlEvents()
            
        })
    
    }
    
    func startSlider(){
        _ = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: Selector("UpdateSlider"), userInfo: nil, repeats: true )
    }

    func MPplayer(){
        let atwork = MPMediaItemArtwork(image: self.cover)
        let duration = self.appDelegate.player!.currentTrackDuration
        _ = MPMediaType.Music
        dispatch_async(dispatch_get_main_queue(), {
            self.mpic.nowPlayingInfo = [
                MPMediaItemPropertyTitle:self.appDelegate.player?.currentTrackMetadata["SPTAudioStreamingMetadataTrackName"] as! String,
                MPMediaItemPropertyArtist:self.appDelegate.player?.currentTrackMetadata["SPTAudioStreamingMetadataArtistName"] as! String,
                MPMediaItemPropertyArtwork:atwork,
                MPMediaItemPropertyPlaybackDuration: duration,
            ]
        })

    }
    
    @IBAction func prev(sender: AnyObject) {
        self.prev()
    }
    
    func prev(){
        self.appDelegate.player?.skipPrevious({ (error:NSError!) -> Void in


        })

    }
    
    @IBAction func play(sender: AnyObject) {
        self.play()
    }
    
    func play(){
        if (self.appDelegate.player!.isPlaying == true){
            self.appDelegate.player?.setIsPlaying(false, callback: { (error:NSError!) -> Void in
                if (error != nil){
                    print(error.localizedDescription)
                    return
                }
                let context = ["play":"pause"]
                if self.Wsession.reachable{
                    self.Wsession.sendMessage(context, replyHandler: { (respuesta:[String : AnyObject]) -> Void in
                        }) { (error:NSError!) -> Void in
                            print("\(error.code) - \(error.localizedDescription)")
                    }
                }
            })
        }else{
            self.appDelegate.player?.setIsPlaying(true, callback: { (error:NSError!) -> Void in
                if (error != nil){
                    print(error.localizedDescription)
                    return
                }
                let context = ["pause":"play"]
                if self.Wsession.reachable{
                    self.Wsession.sendMessage(context, replyHandler: { (respuesta:[String : AnyObject]) -> Void in
                        }) { (error:NSError!) -> Void in
                            print("\(error.code) - \(error.localizedDescription)")
                    }
                }


            })
            
        }

    }

    @IBAction func next(sender: AnyObject) {
        self.next()
    }
    
    func next(){
        self.appDelegate.player?.skipNext({ (error:NSError!) -> Void in

        })

    }
    
    @IBAction func slider(sender: AnyObject) {
        self.Changeslider()
    }
    
    func Changeslider(){
        let position = NSTimeInterval(self.slider.value)
        self.appDelegate.player?.seekToOffset(position, callback: { (error:NSError!) -> Void in
            if (error != nil){
                print("error slider \(error.localizedDescription)")
                return
            }
            dispatch_async(dispatch_get_main_queue(), {
                self.mpic.nowPlayingInfo![MPNowPlayingInfoPropertyPlaybackRate] = position
            })
        })
    }
    
    func durationtime(time:NSNumber){
        let minutes = floor(Float(time)/60)
        let seconds = round(Float(time) - minutes * 60)
        let current = "\(Int(minutes)):\(Int(seconds))"
        dispatch_async(dispatch_get_main_queue(), {
            self.duracion.text = current
        })
    }
    
    func UpdateSlider(){
        let minutes = floor(self.appDelegate.player!.currentPlaybackPosition/60)
        let seconds = round(self.appDelegate.player!.currentPlaybackPosition - minutes * 60)
        let current = "\(Int(minutes)):\(Int(seconds))"
        self.currentTime.text = current
        self.slider.value = Float((self.appDelegate.player?.currentPlaybackPosition)!)
    }
    
    func updateAbumCover(uri:String){
        SPTAlbum.albumWithURI(NSURL(string: uri), session: self.session, callback:  { (error:NSError!, album:AnyObject!) -> Void in
            if (error != nil){
                print(error.localizedDescription)
                return
            }
            self.UpdateCover(album.largestCover)
        })
    }
        
    
    func changetrack(notification: NSNotification){
        let Ntrack = notification.userInfo!["track"] as! SPTPartialTrack
        self.appDelegate.partial = ["track":Ntrack]
        let track = self.appDelegate.player?.currentTrackMetadata["SPTAudioStreamingMetadataTrackName"] as! String
        let index = Int((self.appDelegate.player?.currentTrackIndex)!)
        let context = ["track":track, "index":index, "id": Ntrack.identifier] as [String : AnyObject]
        if self.Wsession.reachable {
            self.Wsession.sendMessage(context, replyHandler: { (respuesta:[String : AnyObject]) -> Void in
                }) { (error:NSError!) -> Void in
                    print("\(error.code) - \(error.localizedDescription)")
            }
        }
        self.slider.maximumValue = Float((self.appDelegate.player?.currentTrackDuration)!)
        self.durationtime(Float((self.appDelegate.player?.currentTrackDuration)!))
        
        self.setTrackInfo_s(Ntrack)
        self.MPplayer()
        self.startSlider()
        
        do {
            try WCSession.defaultSession().updateApplicationContext(context)
            print("enviado")
        }
        catch {
            print("error")
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}


