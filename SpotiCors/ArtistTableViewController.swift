//
//  ArtistTableViewController.swift
//  SpotiCors
//
//  Created by Jonathan on 21/02/16.
//  Copyright © 2016 Jonathan. All rights reserved.
//

import UIKit

class ArtistTableViewController: UITableViewController {
    var session:SPTSession!
    var PartialTracklist:SPTPartialArtist!
    var image:UIImage!
    var artist:SPTArtist!
    var user:SPTUser!
    var topTracks = [SPTTrack]()
    let titulos = ["", "Top tracks", "Albumes"]
    var PartialAlbumes = [SPTPartialAlbum]()
    let appDelegate = UIApplication.sharedApplication().delegate! as! AppDelegate
    var item_select:Int! = 0
    var uris = [NSURL]()

    @IBOutlet var ArtistTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.GetFullArtist()
        self.user = self.appDelegate.user
    }
    
    func GetFullArtist(){
        SPTArtist.artistWithURI(self.PartialTracklist.uri, accessToken: self.session.accessToken) { (error:NSError!, artist:AnyObject!) -> Void in
            if (error != nil){
                print(error.localizedDescription)
                return
            }
            self.artist = artist as! SPTArtist
            if (self.artist.largestImage != nil){
                let data = NSData(contentsOfURL: self.artist.largestImage.imageURL!)
                let image = UIImage(data: data!)!
                self.image = image
            }
            self.getTopTrakcs(self.user as SPTUser)
            self.getAlmbums(self.user as SPTUser)
            self.tableView.reloadData()


        }
    }
    
    func getTopTrakcs(user:SPTUser){
        self.artist.requestTopTracksForTerritory(user.territory, withSession: self.session) { (error:NSError!, toptracks:AnyObject!) -> Void in
            if(error != nil){
                print(error.localizedDescription)
                return
            }
            self.topTracks.removeAll()
            self.uris.removeAll()
            let lista = toptracks as! NSArray
            for (var i = 0; i < lista.count; i++){
                let item = lista[i] as! SPTTrack
                self.topTracks.append(item)
                self.uris.append(item.uri)
            }
            self.tableView.reloadData()
        }
    }
    
    
    func getAlmbums(user:SPTUser){
        self.artist.requestAlbumsOfType(SPTAlbumType.Album, withSession: self.session, availableInTerritory: user.territory) { (error:NSError!, albumes:AnyObject!) -> Void in
            if(error != nil){
                print(error.localizedDescription)
                return
            }
            self.PartialAlbumes.removeAll()
            if (albumes.range.length > 0){
                for (var i = 0; i < albumes.tracksForPlayback().count; i++){
                    let arr = albumes.tracksForPlayback()[i]
                    self.PartialAlbumes.append(arr as! SPTPartialAlbum)
                }
                self.tableView.reloadData()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if let names = self.titulos as [String]?
        {
            return names[section]
        }
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.titulos.count
    }

    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }
        else if section == 1{
            return self.topTracks.count
        }
        else {
            return self.PartialAlbumes.count
        }
    }

    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cellIdentifier = ""
        switch indexPath.section {
        case 0:
            cellIdentifier = "ImageCell"
        case 1:
            cellIdentifier = "TextCell"
        case 2:
            cellIdentifier = "cell"
        default: ()
        }
        
        let cell:ImageCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as! ImageCell
       
        if (self.PartialTracklist != nil) {
            cell.backgroundColor = UIColor.blackColor()
            cell.textLabel?.textColor = UIColor.whiteColor()
            if cellIdentifier == "ImageCell"{
                cell.cover.image = self.image
            }
            if cellIdentifier == "TextCell" {
                if !self.topTracks.isEmpty{
                let partial = self.topTracks[indexPath.row] as SPTPartialTrack
                var totalartist = [String]()
                for(var i = 0; i < partial.artists.count; i++){
                    totalartist.append(partial.artists[i].name)
                }
                let stringRepresentation = totalartist.joinWithSeparator(", ")
                cell.textLabel?.text = self.topTracks[indexPath.row].name
                cell.detailTextLabel?.text = stringRepresentation
                cell.detailTextLabel?.textColor = UIColor.whiteColor()
                }
            }
            if cellIdentifier == "cell"{
                let partial = self.PartialAlbumes[indexPath.row] as SPTPartialAlbum
                cell.textLabel?.text = partial.name
            }
            
        }
        

        
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return UITableViewAutomaticDimension
        default: ()
        return 50
        }
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0){
            return 0.0
        }
        else{
            return 25
        }
    }
    
    override func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 200
        default: ()
        return 50
        }
    }
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        if let imageCell:ImageCell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0)) as? ImageCell {
            imageCell.scrollViewDidScroll(scrollView)
        }
    }
    
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if(indexPath.section == 1){
            self.item_select = indexPath.row
            self.performSegueWithIdentifier("artistPlayer", sender: item_select)
        }
        
        
        if(indexPath.section == 2){
            self.item_select = indexPath.row
            self.performSegueWithIdentifier("artistAlbum", sender: item_select)
        }

    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "artistAlbum") {
            if let AlbumTableViewController = segue.destinationViewController as? AlbumTableViewController {
                AlbumTableViewController.session = self.session
                AlbumTableViewController.PartialAlbum = self.PartialAlbumes[item_select]
            }
        }
        if(segue.identifier == "artistPlayer") {
            if let PlayerViewController = segue.destinationViewController as? PlayerViewController {
                PlayerViewController.session = self.session
                PlayerViewController.track_s = self.topTracks[item_select]
                PlayerViewController.uris = self.uris
                PlayerViewController.index = Int32(item_select)
            }
            
        }

    }


    
    
}

