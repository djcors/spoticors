//
//  BacktableVC.swift
//  SpotiCors
//
//  Created by Jonathan on 8/02/16.
//  Copyright © 2016 Jonathan. All rights reserved.
//

import UIKit

class BacktableVC: UIViewController {
    
    var session:SPTSession!
    var user:SPTUser!
    let userDefaults = NSUserDefaults.standardUserDefaults()
    let appDelegate = UIApplication.sharedApplication().delegate! as! AppDelegate
    
    @IBOutlet weak var User_name: UILabel!
    @IBOutlet weak var Image_user: UIImageView!
    
    var window: UIWindow?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "login_succes", name: "loginSuccessful", object: session)
        if let sessionObj:AnyObject = userDefaults.objectForKey("SpotifySession"){
            let data = sessionObj as! NSData
            self.session = NSKeyedUnarchiver.unarchiveObjectWithData(data) as! SPTSession
        }
        self.user = self.appDelegate.user
        self.User_name.text = self.user.displayName
        
        if (user.smallestImage != nil){
            let data = NSData(contentsOfURL: user.smallestImage.imageURL!)
            self.Image_user.image = UIImage(data: data!)
        }
        
        self.Image_user.layer.cornerRadius = self.Image_user.frame.size.width / 2;
        self.Image_user.clipsToBounds = true;
        self.Image_user.layer.borderWidth = 3.0;
        self.Image_user.layer.borderColor = UIColor.whiteColor().CGColor;
        
        self.Image_user.layer.shadowColor = UIColor.blackColor().CGColor
        self.Image_user.layer.shadowOffset = CGSizeMake(0, 0)
        self.Image_user.layer.shadowRadius = 10
        self.Image_user.layer.shadowOpacity = 0.8


        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        
        // 3
        let color1 = UIColor(red: 3.0/255.0, green: 46.0/255.0, blue: 58.0/255.0, alpha: 1.0).CGColor as CGColorRef
        let color2 = UIColor(red: 2.0/255.0, green: 28.0/255.0, blue: 45.0/255.0, alpha: 1.0).CGColor as CGColorRef
        gradientLayer.colors = [color1, color2]
        
        // 4
        gradientLayer.locations = [0.0, 1.0]
        
        // 5
        self.view.layer.insertSublayer(gradientLayer, atIndex: 0)
        self.view.backgroundColor = UIColor.blackColor()

    }
    @IBAction func logout(sender: AnyObject) {
        self.session = nil
        userDefaults.setObject(nil, forKey: "SpotifySession")
        userDefaults.synchronize()
        self.appDelegate.player?.stop({ (error:NSError!) -> Void in
            if (error != nil){
                print(error.localizedDescription)
            }
        })
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewControllerWithIdentifier("login") as! LoginViewController
        UIApplication.sharedApplication().keyWindow?.rootViewController = viewController;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source



    
    

}
