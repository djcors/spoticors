//
//  TrackListController.swift
//  SpotiCors
//
//  Created by Jonathan on 11/02/16.
//  Copyright © 2016 Jonathan. All rights reserved.
//

import UIKit

class TrackListController: UITableViewController {
    @IBOutlet weak var TracklistTable: UITableView!
    var session:SPTSession!
    var PartialTracklist:SPTPartialPlaylist!
    var TrackList: SPTPlaylistSnapshot!
    var total:Int!
    var total_tracks:Int!
    var image:UIImage!
    var item_select:Int!
    var uris = [NSURL]()
    var arrary_tracks = [SPTPlaylistTrack]()
    
        override func viewDidLoad() {
            super.viewDidLoad()
            self.getPlayList()
            self.title = self.PartialTracklist.name
            self.tableView.backgroundColor = UIColor.blackColor()
            let appDelegate = UIApplication.sharedApplication().delegate! as! AppDelegate
            appDelegate.window?.backgroundColor = UIColor.blackColor()
            self.view.backgroundColor = UIColor.blackColor()
            let cantidad = self.PartialTracklist.trackCount / 100
            self.total_tracks = cantidad.toInt
            
        }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else{
            return self.total
        }
    }
    
    func getPlayList(){
        SPTPlaylistSnapshot.playlistWithURI(self.PartialTracklist.uri, session: self.session) { (error:NSError!, pl:AnyObject!) -> Void in
            if (error != nil){
                print("error al traer playlist \(error.localizedDescription)")
                return
            }
            if pl.trackCount > 0 {
            self.TrackList = pl as! SPTPlaylistSnapshot
                for(var i = 0; i < self.TrackList.firstTrackPage.items.count; i++){
                    let arr = self.TrackList.firstTrackPage.items[i] as! SPTPlaylistTrack
                    self.arrary_tracks.append(arr)
                    self.uris.append(arr.uri)
                }
            
            

            if (self.TrackList.firstTrackPage.hasNextPage){
                    self.TrackList.firstTrackPage.requestNextPageWithSession(self.session, callback: { (error:NSError!, page:AnyObject!) -> Void in
                        if(error != nil){
                            print(error.localizedDescription)
                            return
                        }
                        for(var i = 0; i < page.tracksForPlayback().count; i++){
                            let arr = page.tracksForPlayback()[i] as! SPTPlaylistTrack
                            self.arrary_tracks.append(arr)
                            self.uris.append(arr.uri)
                        }
                        if (page.hasNextPage == true){
                            page.requestNextPageWithSession(self.session, callback: { (error:NSError!, page2:AnyObject!) -> Void in
                                if (error != nil){
                                    print(error.localizedDescription)
                                    return
                                }
                                for(var i = 0; i < page2.tracksForPlayback().count; i++){
                                    let arr = page2.tracksForPlayback()[i] as! SPTPlaylistTrack
                                    self.arrary_tracks.append(arr)
                                    self.uris.append(arr.uri)
                                }
                            })
                            
                        }
                        
                    })
                }
                
                if (self.TrackList.largestImage != nil){
                    let data = NSData(contentsOfURL: self.TrackList.largestImage.imageURL!)
                    let image = UIImage(data: data!)!
                    self.image = image
                }
                self.tableView.reloadData()
            }

        }
    
    }
    

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        
        var cellIdentifier = ""
        switch indexPath.section {
        case 0:
            cellIdentifier = "ImageCell"
        case 1:
            cellIdentifier = "TextCell"
        default: ()
        }
        
        let cell:ImageCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as! ImageCell
        if (self.TrackList != nil) {
            cell.backgroundColor = UIColor.blackColor()
            cell.textLabel?.textColor = UIColor.whiteColor()
            
            if cellIdentifier == "TextCell"{
                let partial = self.arrary_tracks[indexPath.row] as SPTPartialTrack
                cell.textLabel?.text = partial.name
                
                var totalartist = [String]()
                for(var i = 0; i < partial.artists.count; i++){
                    totalartist.append(partial.artists[i].name)
                }
                let stringRepresentation = totalartist.joinWithSeparator(", ")
                cell.detailTextLabel?.text = stringRepresentation
                cell.detailTextLabel?.textColor = UIColor.whiteColor()

                
            }
            if cellIdentifier == "ImageCell"{
                cell.cover.image = self.image
            }
            
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return UITableViewAutomaticDimension
        default: ()
        return 50
        }
    }
    
    override func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 200
        default: ()
        return 50
        }
    }
    
    // MARK: - Scroll view delegate
    
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        if let imageCell:ImageCell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0)) as? ImageCell {
            imageCell.scrollViewDidScroll(scrollView)
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        item_select = indexPath.row
        self.performSegueWithIdentifier("player", sender: item_select)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "player") {
            if let PlayerViewController = segue.destinationViewController as? PlayerViewController {
                PlayerViewController.session = self.session
                PlayerViewController.track = self.arrary_tracks[item_select]
                PlayerViewController.uris = self.uris
                PlayerViewController.index = Int32(item_select)
            }
        }
    }

}

class ImageCell: UITableViewCell {
    

    @IBOutlet weak var cover: UIImageView!
    @IBOutlet weak var bottomSpaceConstraint: NSLayoutConstraint!
    @IBOutlet weak var topSpaceConstraint: NSLayoutConstraint!
    @IBOutlet weak var containerView: UIView!
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if scrollView.contentOffset.y >= 0 {
            // scrolling up
            containerView.clipsToBounds = true
            bottomSpaceConstraint?.constant = -scrollView.contentOffset.y / 2
            topSpaceConstraint?.constant = scrollView.contentOffset.y / 2
        } else {
            // scrolling down
            topSpaceConstraint?.constant = scrollView.contentOffset.y
            containerView.clipsToBounds = false
        }
    }
}
