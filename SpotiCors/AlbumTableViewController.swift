//
//  AlbumTableViewController.swift
//  SpotiCors
//
//  Created by Jonathan on 6/03/16.
//  Copyright © 2016 Jonathan. All rights reserved.
//

import UIKit

class AlbumTableViewController: UITableViewController {

    @IBOutlet var albumTable: UITableView!
    
    var session:SPTSession!
    let appDelegate = UIApplication.sharedApplication().delegate! as! AppDelegate
    var image:UIImage!
    var PartialAlbum:SPTPartialAlbum!
    var total:Int!
    var PartialTracklist = [SPTPartialTrack]()
    var album:SPTAlbum!
    var uris = [NSURL]()
    var item_select:Int = 0
    var tracks = [SPTPartialTrack]()
    var track:SPTPartialTrack!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.blackColor()
        self.albumTable.backgroundColor = UIColor.blackColor()
        self.getFullAlbum()
        if (self.PartialAlbum.largestCover != nil){
            let data = NSData(contentsOfURL: self.PartialAlbum.largestCover.imageURL)
            let image = UIImage(data: data!)!
            self.image = image
        }

    }
    
    func getFullAlbum(){
        SPTAlbum.albumWithURI(self.PartialAlbum.uri, session: self.session) { (error:NSError!, album:AnyObject!) -> Void in
            if (error != nil){
                print(error.localizedDescription)
                return
            }
            let album = album  as! SPTAlbum
            for (var i = 0; i < album.firstTrackPage.items.count; i++){
                let arr = album.firstTrackPage.items[i] as! SPTPartialTrack
                self.PartialTracklist.append(arr)
                self.uris.append(arr.uri)
            }
            self.getTrack()
            self.albumTable.reloadData()
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }

    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }
        else {
            return self.PartialTracklist.count
        }
    }


    

    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return UITableViewAutomaticDimension
        default: ()
        return 50
        }
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    override func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 200
        default: ()
        return 50
        }
    }
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        if let imageCell:ImageCell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0)) as? ImageCell {
            imageCell.scrollViewDidScroll(scrollView)
        }
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cellIdentifier = ""
        switch indexPath.section {
        case 0:
            cellIdentifier = "ImageCell"
        case 1:
            cellIdentifier = "TextCell"
        default: ()
        }
        
        
        let cell:ImageCell = self.albumTable.dequeueReusableCellWithIdentifier(cellIdentifier) as! ImageCell
        cell.backgroundColor = UIColor.blackColor()
        cell.textLabel?.textColor = UIColor.whiteColor()
        
        if cellIdentifier == "ImageCell"{
            cell.cover.image = self.image
        }
        
        if cellIdentifier == "TextCell" {
            if !self.PartialTracklist.isEmpty{
                let partial = self.PartialTracklist[indexPath.row] as SPTPartialTrack
                var totalartist = [String]()
                for(var i = 0; i < partial.artists.count; i++){
                    totalartist.append(partial.artists[i].name)
                }
                let stringRepresentation = totalartist.joinWithSeparator(", ")
                cell.textLabel?.text = partial.name
                cell.detailTextLabel?.text = stringRepresentation
                cell.detailTextLabel?.textColor = UIColor.whiteColor()
            }
        }


        
        return cell
    }
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        item_select = indexPath.row
        self.performSegueWithIdentifier("playerAlbum", sender: item_select)
    }
    
    
    func getTrack(){
        SPTTrack.tracksWithURIs(self.uris, session: self.session) { (error:NSError!, tracks:AnyObject!) -> Void in
            if (error != nil){
                print(error.localizedDescription)
                return
            }
            self.tracks = tracks as! [SPTPartialTrack]
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "playerAlbum") {
            if let PlayerViewController = segue.destinationViewController as? PlayerViewController {
                PlayerViewController.session = self.session
                PlayerViewController.uris = self.uris
                PlayerViewController.index = Int32(self.item_select)
                PlayerViewController.track_s = self.tracks[self.item_select]
            }
        }
    }

    


}
