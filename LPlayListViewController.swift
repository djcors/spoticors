//
//  LPlayListViewController.swift
//  SpotiCors
//
//  Created by Jonathan on 8/02/16.
//  Copyright © 2016 Jonathan. All rights reserved.
//

import UIKit


class LPlayListViewController: UITableViewController {

    
    @IBOutlet weak var Open: UIBarButtonItem!
    var session:SPTSession!
    let userDefaults = NSUserDefaults.standardUserDefaults()
    var total:Int! = 1
    var playlists:SPTPlaylistList!
    var item_select:Int = 0
    var images = [UIImage]()
    var images_cache = [UIImage]()
    var arrary_tracks = [SPTPartialPlaylist]()
    
    @IBOutlet var lists: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.blackColor()
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.session = appDelegate.session
        //carga icono para el menu
        let myBtn: UIButton = UIButton()
        myBtn.setImage(UIImage(named: "menu"), forState: .Normal)
        myBtn.frame = CGRectMake(0, 0, 30, 70)
        myBtn.addTarget(self.revealViewController(), action: "revealToggle:", forControlEvents: .TouchUpInside)
        self.navigationItem.setLeftBarButtonItem(UIBarButtonItem(customView: myBtn), animated: true)
        //gestos
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.tableView.backgroundColor = UIColor.blackColor()
        self.view.backgroundColor = UIColor.blackColor()
        self.getPlayLists()
        //self.refreshControl?.addTarget(self, action: "handleRefresh:", forControlEvents: UIControlEvents.ValueChanged)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func getPlayLists(){
        
        SPTPlaylistList.playlistsForUserWithSession(self.session) { (error:NSError!, playlist:AnyObject!) -> Void in
            if error != nil {
                print(error.localizedDescription)
                return
            }
            let page = playlist as! SPTListPage

            if page.totalListLength > 0{
                for(var e = 0; e < page.items.count; e++){
                    let arr = page.tracksForPlayback()[e] as! SPTPartialPlaylist
                    self.arrary_tracks.append(arr)
                }
                if page.hasNextPage{
                    self.getNewpage(page)
                }else{
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                        self.total = self.arrary_tracks.count
                        for (var i = 0; i < self.arrary_tracks.count; i++){
                            let partial = self.arrary_tracks[i]
                            let image:UIImage!
                            /*
                            if (partial.smallestImage != nil){
                                let data = NSData(contentsOfURL: partial.smallestImage.imageURL!)
                                image = UIImage(data: data!)!
                            }
                            else{
                                image = UIImage(named: "play")
                            }
                            self.images.append(image!)
                            */
                        }
                        
                        dispatch_async(dispatch_get_main_queue()) {
                            self.lists.reloadData()
                        }
                    })
                }
            }
            
            

            

                
                
                
            
        }
        
    }
    
    
    func getNewpage(var actual:SPTListPage){
        actual.requestNextPageWithSession(self.session) { (error:NSError!, page:AnyObject!) -> Void in
            if (error != nil){
                print(error.localizedDescription)
                return
            }
            actual = page as! SPTListPage

            for(var e = 0; e < actual.items.count; e++){
                let arr = actual.items[e] as! SPTPartialPlaylist
                self.arrary_tracks.append(arr)
            }
            if actual.hasNextPage{
                self.getNewpage(actual)
            }
            else{
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                    self.total = self.arrary_tracks.count

                        for (var i = 0; i < self.arrary_tracks.count; i++){
                            let partial = self.arrary_tracks[i]
                            /*
                            let image:UIImage!
                            if (partial.smallestImage != nil){
                                let data = NSData(contentsOfURL: partial.smallestImage.imageURL!)
                                image = UIImage(data: data!)!
                            }
                            else{
                                image = UIImage(named: "play")
                            }
                            self.images.append(image!)
                            */
                        }
                    dispatch_async(dispatch_get_main_queue()) {
                        self.lists.reloadData()
                    }
                })
            }
        }
    }
    
    
    override func tableView(tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return self.total
    }
    
    
    override func tableView(tableView:UITableView, cellForRowAtIndexPath indexPath:NSIndexPath) -> UITableViewCell{
        
        var cell = self.lists.dequeueReusableCellWithIdentifier("cell")
        
        if (cell != nil)
        {
            cell = UITableViewCell(style: UITableViewCellStyle.Subtitle,
                reuseIdentifier: "cell")
        }
        cell!.backgroundColor = UIColor.blackColor()
        cell!.textLabel?.textColor = UIColor.whiteColor()
        cell!.detailTextLabel?.textColor = UIColor.darkGrayColor()
        if (self.arrary_tracks.count > 0) {
                let partial = self.arrary_tracks[indexPath.row]
                let total_tracks = partial.trackCount.toInt
                let total_canciones = String("\(total_tracks) Canciones")
                //cell!.imageView?.image = self.images[indexPath.row]
                

                cell!.textLabel?.text = partial.name
                cell!.detailTextLabel?.text = total_canciones
                cell!.accessoryType = .DisclosureIndicator
                
            
        }
        
        
                
        
        
        
        
        return cell!
        
        
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        item_select = indexPath.row
        self.performSegueWithIdentifier("tracklist", sender: item_select)
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "tracklist") {
            if let TrackListController = segue.destinationViewController as? TrackListController {
                TrackListController.session = self.session
                TrackListController.PartialTracklist = self.arrary_tracks[item_select]
                TrackListController.total = self.arrary_tracks[item_select].trackCount.toInt
            }
        }
    }
}


extension UInt {
    /// SwiftExtensionKit
    var toInt: Int { return Int(self) }
}
